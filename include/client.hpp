#ifndef INCLUDE_CLIENT_HPP_
#define INCLUDE_CLIENT_HPP_

#include <QObject>

#include <boost/asio.hpp>

#include <queue>
#include <fstream>

#include "common.hpp"
#include "packet.hpp"

using boost::asio::ip::tcp;
using boost::asio::io_service;
using boost::system::error_code;

namespace io = boost::asio;

class Client : public QObject
{
    Q_OBJECT
public:
    Client(const std::string &ip, const unsigned short port);

	~Client();

public Q_SLOTS:
    void onStart();
    void onStop();

    void onSendMessage(const QString& message);
    void onSendImage(const QString& filePath);

Q_SIGNALS:
    void stop();
    void connectSuccess();
    void connectFailure(const QString& errorMsg);
    void messageRecived(const QString& msg);
    void imageRecived(const QString& imagePath);

private:
    void handleWriteBody(boost::system::error_code const& ec, std::size_t bytesTransferred);
    void handleWriteHeader(boost::system::error_code const& ec, std::size_t bytesTransferred);

    void asyncReadMessage();
    void onRead(boost::system::error_code ec, size_t bytesTransferred);
    void asyncWriteMessage();
    void onWrite(boost::system::error_code ec, size_t bytesTransferred);

    void asyncWriteHeader();
    void asyncReadHeader();

    void asyncReadImage();
    void handleReadUntil(boost::system::error_code const& ec, std::size_t bytesTransferred);
    void handleRead(boost::system::error_code const& ec, std::size_t bytesTransferred);

    io::io_context m_ioContext {};
    io::ip::tcp::resolver::results_type m_endpoints;
    tcp::socket m_socket;
    io::streambuf m_streamBuf;

    std::queue<std::string> m_outgoingMessages {};

    Packet m_packet;

    //for image
    std::ifstream m_ifs;
    unsigned long long m_fileSize;
    unsigned long long m_sentFileBody = 0;
    char m_fileBuf[4096];

    std::string m_imgPath;

    //for images
    io::streambuf m_sBuf;
    std::istream m_istr{&m_sBuf};
    std::size_t m_sizeBody{0};
    std::ofstream m_ofs;
    char m_buf[1024];

    std::string m_lastImagePath;

    enum MessageTypes
    {
        SendBroadcastMessage,
        SendBroadcastImage,
        GetMessageFromServer,
        GetImageFromServer
    };
};

#endif /* INCLUDE_CLIENT_HPP_ */
