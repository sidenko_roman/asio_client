#ifndef CONTROLLER_H
#define CONTROLLER_H
#include "client.hpp"

#include <QObject>
#include <QSharedPointer>
#include <QThread>

class Controller : public QObject
{
    Q_OBJECT
    Q_DISABLE_COPY(Controller)
public:
    Controller(QObject* parent = nullptr);

public:
    Q_INVOKABLE void connectToServer(const QString& ip, const unsigned short port);
    Q_INVOKABLE void sendMessage(const QString& message);
    Q_INVOKABLE void sendImage(const QUrl& url);

Q_SIGNALS:
    void connectSuccess();
    void connectFailure(const QString& errorMsg);

    void messageRecived(const QString& msg);
    void imageRecived(const QString& imagePath);
    void start();

    void sendMessageSignal(const QString& message);
    void sendImageSignal(const QString& imagePath);
private:
    QScopedPointer<Client> m_client;
    QThread m_clientThread;
};

#endif // CONTROLLER_H
