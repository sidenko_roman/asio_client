#include "controller.hpp"
#include "logger.hpp"

#include <QUrl>

Controller::Controller(QObject* parent)
    : QObject(parent)
{
    m_clientThread.setObjectName("Client thread");

}

void Controller::connectToServer(const QString& ip, const unsigned short port)
{
    QThread* thread = new QThread();
    m_client.reset(new Client(ip.toStdString(), port));
    m_client->moveToThread(thread);

    connect(m_client.get(), &Client::connectSuccess, this, &Controller::connectSuccess);
    connect(m_client.get(), &Client::connectFailure, this, &Controller::connectFailure);
    connect(m_client.get(), &Client::messageRecived, this, &Controller::messageRecived);
    connect(m_client.get(), &Client::imageRecived, this, & Controller::imageRecived);

    connect(this, &Controller::sendMessageSignal, m_client.get(), &Client::onSendMessage, Qt::DirectConnection);
    connect(this, &Controller::sendImageSignal, m_client.get(), &Client::onSendImage, Qt::DirectConnection);

    connect(m_client.get(), &Client::destroyed, thread, &QThread::quit);
    connect(thread, &QThread::finished, thread, &QThread::deleteLater);
    connect(thread, &QThread::started, m_client.get(), &Client::onStart);
    thread->start();
}

void Controller::sendMessage(const QString& message)
{
    lockStream();
    LOG_INF() << "Message to send: " << message.toStdString() << std::endl;
    unlockStream();
    emit sendMessageSignal(message);
}

void Controller::sendImage(const QUrl& url)
{
    lockStream();
    LOG_INF() << "Image to send: " << url.toLocalFile().toStdString() << std::endl;
    unlockStream();
    emit sendImageSignal(url.toLocalFile());
}
