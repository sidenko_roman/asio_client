#include <QGuiApplication>
#include <QQmlApplicationEngine>
#include <QQmlContext>

#include "common.hpp"
#include "client.hpp"
#include "controller.hpp"

int main(int argc, char *argv[])
{
#if QT_VERSION < QT_VERSION_CHECK(6, 0, 0)
    QCoreApplication::setAttribute(Qt::AA_EnableHighDpiScaling);
#endif

    QGuiApplication app(argc, argv);

    app.setOrganizationName("CHI");
    app.setOrganizationDomain("CHI");

    QQmlApplicationEngine engine;
    Controller* g_controller = new Controller;

    qmlRegisterSingletonInstance<Controller>("controllers", 1, 0, "Controller", g_controller);

    const QUrl url(QStringLiteral("qrc:/main.qml"));
    QObject::connect(&engine, &QQmlApplicationEngine::objectCreated,
                     &app, [url](QObject *obj, const QUrl &objUrl) {
        if (!obj && url == objUrl)
            QCoreApplication::exit(-1);
    }, Qt::QueuedConnection);
    engine.load(url);

    return app.exec();
}
