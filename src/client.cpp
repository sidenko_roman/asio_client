#include "client.hpp"
#include "packet.hpp"

#include <iostream>
#include <string>
#include <cstdlib>

#include <boost/regex.hpp>
#include <boost/filesystem.hpp>

using boost::asio::ip::address;
using boost::system::error_code;
using boost::asio::placeholders::error;
using boost::asio::buffer;

namespace fs = boost::filesystem;

Client::Client(const std::string& ip, const unsigned short port)
    : m_socket(m_ioContext)
{
    io::ip::tcp::resolver resolver {m_ioContext};
    m_endpoints = resolver.resolve(ip, std::to_string(port));

    LOG_INF() << "Initiating client..." << std::endl;
}

Client::~Client()
{
    onStop();

    LOG_INF() << "Client exited successfully!" << std::endl;
}

void Client::onStart()
{
    lockStream();
    LOG_INF() << "Connecting to server..." << std::endl;
    unlockStream();

    //TODO add deadline for connection
    io::async_connect(m_socket, m_endpoints, [this](boost::system::error_code ec, io::ip::tcp::endpoint ep)
    {
        if (!ec)
        {
            lockStream();
            LOG_INF() << "Successfully connected to the server!" << std::endl;
            unlockStream();
            emit connectSuccess();
            asyncReadHeader();
        }
    });

    m_ioContext.run();
}

void Client::onStop ()
{
    boost::system::error_code ec;
    m_socket.close(ec);

    if (ec)
    {
        // process error
    }

    emit stop();
}

void Client::onSendMessage(const QString& message)
{
    m_packet.set(MSG_VERSION, MessageTypes::SendBroadcastMessage, message.toStdString());
    asyncWriteHeader();
}

void Client::handleWriteBody(boost::system::error_code const& ec, std::size_t bytesTransferred)
{
    if (ec)
    {
        lockStream();
        LOG_INF() << ec << ", " << ec.message() << std::endl;
        unlockStream();
        return;
    }
    lockStream();
    LOG_INF() << "m_sentFileBody : " << m_sentFileBody << " m_fileSize " << m_fileSize << std::endl;
    unlockStream();
    m_sentFileBody += bytesTransferred;
    if (m_ifs)
    {
        m_ifs.read(m_fileBuf, std::size(m_fileBuf));
        boost::asio::async_write(m_socket, boost::asio::buffer(m_fileBuf, m_ifs.gcount()),
                                 [this](boost::system::error_code const& ec, std::size_t bytesTransferred){handleWriteBody(ec, bytesTransferred);
                             });
    }
    else
    {
        if (m_sentFileBody != m_fileSize)
        {
            lockStream();
            LOG_INF() << "sentFileBody != fileSize\n" << std::endl;
            unlockStream();
            return;
        }
        lockStream();
        LOG_INF() << "sentFileBody: " << m_sentFileBody << std::endl;
        LOG_INF() << "OK" << std::endl;
        unlockStream();
        m_sentFileBody = 0;
        m_ifs.close();
    }
}

void Client::handleWriteHeader(boost::system::error_code const& ec, std::size_t bytesTransferred)
{
    if (ec)
    {
        lockStream();
        LOG_INF() << ec << ", " << ec.message() << std::endl;
        unlockStream();
        return;
    }

    m_ifs.read(m_fileBuf, std::size(m_fileBuf));
    boost::asio::async_write(m_socket, boost::asio::buffer(m_fileBuf, m_ifs.gcount()),
                             [this](boost::system::error_code const& ec, std::size_t bytesTransferred){handleWriteBody(ec, bytesTransferred);
                         });
}

void Client::onSendImage(const QString& filePath)
{
    std::string stdFilePath = filePath.toStdString();
    m_imgPath = stdFilePath;
    m_packet.set(MSG_VERSION, MessageTypes::SendBroadcastImage, stdFilePath);

    asyncWriteHeader();
}

void Client::asyncReadMessage()
{
    io::async_read_until(m_socket, m_streamBuf, "\n", [this](boost::system::error_code ec, size_t bytesTransferred)
    {
        onRead(ec, bytesTransferred);
    });
}

void Client::onRead(boost::system::error_code ec, size_t bytesTransferred)
{
    if (ec)
    {
        onStop();
        return;
    }

    std::stringstream message;
    message << std::istream{&m_streamBuf}.rdbuf();
    std::string messageString = message.str().substr(0, message.str().size()-1);
    emit messageRecived(QString::fromStdString(messageString));
    asyncReadHeader();
}

void Client::asyncWriteMessage()
{
    io::async_write(m_socket, io::buffer(m_outgoingMessages.front()), [this](boost::system::error_code ec, size_t bytesTransferred)
    {
        onWrite(ec, bytesTransferred);
    });
}

void Client::onWrite(boost::system::error_code ec, size_t bytesTransferred)
{
    if (ec)
    {
        onStop();
        return;
    }

    m_outgoingMessages.pop();
    if (!m_outgoingMessages.empty())
    {
        asyncWriteMessage();
    }
}

void Client::asyncWriteHeader()
{
    boost::asio::async_write(m_socket, boost::asio::buffer(&m_packet.header(), m_packet.getHdrSize()),
                             [this](boost::system::error_code ec, std::size_t length)
    {
        if (!ec)
        {
            lockStream();
            LOG_INF() << "Header bytes send: " << length << std::endl;
            unlockStream();

            switch (m_packet.header().m_type)
            {
                case MessageTypes::SendBroadcastMessage:
            {
                lockStream();
                LOG_INF() << "Start do message send work: " << std::endl;
                unlockStream();

                bool queueIdle = m_outgoingMessages.empty();
                std::string sendedStr = m_packet.getMessage();
                sendedStr += "\n"; //added "\n" for use async_read_until
                m_outgoingMessages.push(sendedStr);

                if (queueIdle)
                    asyncWriteMessage();
                break;
            }

            case MessageTypes::SendBroadcastImage:
            {
                lockStream();
                LOG_INF() << "Start do image work" << std::endl;
                unlockStream();

                m_ifs.open(m_imgPath, std::ios::binary);
                if (!m_ifs.is_open())
                {
                    lockStream();
                    LOG_INF() << "Can't read file" << std::endl;
                    unlockStream();
                    return;
                }
                m_fileSize = fs::file_size(m_imgPath);
                lockStream();
                LOG_INF() << "fileSize: " << m_fileSize << std::endl;
                unlockStream();

                std::stringstream ss{ "FileName: " + fs::path{m_imgPath}.filename().string() + "\r\n" + "FileSize: " + std::to_string(m_fileSize) + "\r\n\r\n" };
                ss.getline(m_fileBuf, std::size(m_fileBuf), '*');
                boost::asio::async_write(m_socket, boost::asio::buffer(m_fileBuf, ss.str().size()),
                                         [this](boost::system::error_code const& ec, std::size_t bytesTransferred){handleWriteHeader(ec, bytesTransferred);
                                  });
                break;
            }
            default:
            {
                unlockStream();
                LOG_INF() << "Not supported packet type " << std::endl;
                unlockStream();
                break;
            }
            }
        }
        else
        {
           onStop();
           return;
        }
    });
}

void Client::asyncReadHeader()
{
        io::async_read(m_socket, buffer(&m_packet.header(), m_packet.getHdrSize()), io::transfer_exactly(m_packet.getHdrSize()),
                       [this](boost::system::error_code ec, size_t /*bytesTransferred*/) {
            if (!ec)
            {
                lockStream();
                LOG_INF() << "Header: "
                            << std::hex << m_packet.header().m_version << ", "
                            << std::hex << m_packet.header().m_type    << ", "
                            << std::dec << m_packet.header().m_length  << std::endl;
                unlockStream();
                switch (m_packet.header().m_type)
                {
                    case MessageTypes::GetMessageFromServer:
                {
                    asyncReadMessage();
                    break;
                }

                case MessageTypes::GetImageFromServer:
                {
                    asyncReadImage();
                    break;
                }
                default:
                {
                    lockStream();
                    LOG_INF() << "Unsupported packet type " << std::endl;
                    unlockStream();
                    break;
                }
                }
            }
            else
            {
                lockStream();
                LOG_ERR() << "Error: " << ec.message() << std::endl;
                unlockStream();
            }
        });
}

void Client::asyncReadImage()
{
    boost::asio::async_read_until(m_socket, m_sBuf, "\r\n\r\n",
                                  [this](boost::system::error_code const& ec, std::size_t bytesTransferred){
                                    handleReadUntil(ec, bytesTransferred);
                                  });
}

void Client::handleReadUntil(boost::system::error_code const& ec, std::size_t bytesTransferred)
{
    if (ec && ec != boost::asio::error::eof)
    {
        lockStream();
        LOG_INF() << ec << ", " << ec.message() << std::endl;
        unlockStream();
        return;
    }

    boost::smatch mr;
    boost::regex regFileName("FileName: *(.+?)\r\n");
    boost::regex regFileSize("FileSize: *([0-9]+?)\r\n");
    std::string headers, tmp;
    while (std::getline(m_istr, tmp) && tmp != "\r")
    {
        headers += (tmp + '\n');
    }
    lockStream();
    LOG_INF() << "Headers:\n" << headers << std::endl;
    unlockStream();
    if (!boost::regex_search(headers, mr, regFileSize))
    {
        unlockStream();
        LOG_INF() << "regFileSize not found\n" << std::endl;
        unlockStream();
        return;
    }
    m_fileSize = std::stoull(mr[1]);
    lockStream();
    LOG_INF() << "fileSize = " << m_fileSize << std::endl;
    unlockStream();
    if (!boost::regex_search(headers, mr, regFileName))
    {
        lockStream();
        LOG_INF() << "regFileName not found" << std::endl;
        unlockStream();
        return;
    }
    std::string fileName = mr[1];
    lockStream();
    LOG_INF() << "fileName = " << fileName << std::endl;
    unlockStream();
    m_ofs.open("g:\\TempAsio\\FromServer\\" + fileName, std::ios::binary);
    if (!m_ofs.is_open())
    {
        lockStream();
        LOG_INF() << "Unable to open output file" << std::endl;
        unlockStream();
        return;
    }
    m_lastImagePath = "g:\\TempAsio\\FromServer\\" + fileName;
    lockStream();
    LOG_INF() << "sBuf.size() = " << m_sBuf.size() << std::endl;
    unlockStream();

    if (m_sBuf.size())
    {
        m_sizeBody += m_sBuf.size();
        m_ofs << &m_sBuf;
    }
    if (m_sizeBody != m_fileSize)
    {
        boost::asio::async_read(m_socket, m_sBuf, boost::asio::transfer_at_least(1),
                                [this](boost::system::error_code const& ec, std::size_t bytesTransferred){handleRead(ec, bytesTransferred);
                                });
    }
    else
    {
        lockStream();
        LOG_INF() << "Transfer complete successfully" << std::endl;
        unlockStream();
    }
}

void Client::handleRead(boost::system::error_code const& ec, std::size_t bytesTransferred)
{
    if (ec && ec != boost::asio::error::eof)
    {
        lockStream();
        LOG_INF() << ec << ", " << ec.message() << std::endl;
        unlockStream();
        return;
    }

    m_sizeBody += bytesTransferred;
    m_ofs << &m_sBuf;

    lockStream();
    LOG_INF() << "sizeBody = " << m_sizeBody << std::endl;
    unlockStream();

    if (m_sizeBody != m_fileSize)
    {
        boost::asio::async_read(m_socket, m_sBuf, boost::asio::transfer_at_least(1),
                                [this](boost::system::error_code const& ec, std::size_t bytesTransferred){handleRead(ec, bytesTransferred);
                                });
    }
    else
    {
        lockStream();
        LOG_INF() << "Transfer complete successfully" << std::endl;
        unlockStream();
        lockStream();
        LOG_INF() << m_lastImagePath << std::endl;
        unlockStream();
        m_sizeBody = 0;
        m_ofs.close();
        emit imageRecived(QString::fromStdString(m_lastImagePath));
        asyncReadHeader();
    }
}
