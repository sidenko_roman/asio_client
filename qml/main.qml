import QtQuick 2.15
import QtQuick.Window 2.15
import QtQuick.Controls 2.15
import QtQml.Models 2.1
import QtQuick.Layouts 1.15
import QtQuick.Dialogs 1.3

import controllers 1.0

Window {
    width: 640
    height: 480
    visible: true
    id:mainWindow

    property ListModel chatModel: ListModel{}

    StackView {
        id: stack
        initialItem: startView
        anchors.fill: parent
    }
    Component {
        id: startView
        Item{
            width: 450
            height: 150
            anchors.centerIn: parent
            Column{
                spacing: 10
                Row {
                    spacing: 10
                    TextField {
                        id: textFieldIp
                        validator: RegExpValidator {
                            regExp:  /^((?:[0-1]?[0-9]?[0-9]|2[0-4][0-9]|25[0-5])\.){0,3}(?:[0-1]?[0-9]?[0-9]|2[0-4][0-9]|25[0-5])$/
                        }
                        placeholderText: qsTr("IP Address")
                        text: "127.0.0.1"
                    }
                    TextField {
                        id: textFieldPort
                        validator: RegExpValidator {
                            regExp:  /^([1-9][0-9]{0,3}|[1-5][0-9]{4}|6[0-4][0-9]{3}|65[0-4][0-9]{2}|655[0-2][0-9]|6553[0-5])$/
                        }
                        placeholderText: qsTr("Port")
                        text: "9900"
                    }
                }
                Button {
                    text: qsTr("Connect")
                    anchors.horizontalCenter: parent.horizontalCenter
                    onClicked: Controller.connectToServer(textFieldIp.text, textFieldPort.text)
                }
            }
        }
    }
    Component {
        id: roomView
        Item {
            id: roomChat
            ListView {
                id: chatView
                width: parent.width
                height: parent.height * 0.85
                anchors.top: parent.top
                anchors.left: parent.left

                anchors.margins: 10

                model: mainWindow.chatModel

                delegate: Text {
                    width: parent.width
                    text: msg
                    wrapMode: Text.WordWrap
                }
            }
            RowLayout{
                width: parent.width
                height: parent.height * 0.15
                anchors.bottom: parent.bottom
                anchors.left: parent.left
                TextField {
                    id:textToSend
                    Layout.preferredWidth: 430
                    placeholderText: qsTr("Enter message")
                }
                Button{
                    id:p_openImage
                    Layout.preferredWidth: 100
                    text: qsTr("Open Image")
                    onClicked: fileDialog.open()
                }
                Button{
                    id:sendButton
                    text: qsTr("Send message")
                    Layout.preferredWidth: 100
                    onClicked: {
                        if(textToSend.length > 0)
                            Controller.sendMessage(textToSend.text)
                        textToSend.text = ""
                    }
                }
            }
        }


    }

    Popup {
        property alias popupText: p_txt.text
        id: signalingPopup
        width: 200
        height: 300
        anchors.centerIn: parent
        modal: true
        focus: true
        closePolicy: Popup.CloseOnEscape | Popup.CloseOnPressOutsideParent
        Column{
            anchors.centerIn: parent
            spacing: 10
            Text {
                id: p_txt
            }
            Button{
                id: p_btn
                text: qsTr("Ok")
                anchors.horizontalCenter: parent.horizontalCenter
                onClicked: signalingPopup.close()
            }
        }
    }

    FileDialog {
        id: fileDialog
        title: "Please choose a file"
        folder: shortcuts.pictures
        nameFilters: [ "Image files (*.jpg *.png)"]
        selectMultiple: false
        onAccepted: {
            Controller.sendImage(fileUrl)
        }
    }

    Connections{
        target: Controller
        function onConnectSuccess(){stack.push(roomView)}
        function onConnectFailure(errorMsg){signalingPopup.popupText = errorMsg; signalingPopup.open()}
        function onMessageRecived(msg){
            mainWindow.chatModel.append({msg:msg})
        }
        function onImageRecived(src){
            mainWindow.chatModel.append({msg:"Recived image:"+src})
        }
    }
}
